<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-20 08:49:51
         compiled from "C:\wamp64\www\projet\application\views\header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14043505695ce26a2f5a2428-26120407%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '54c68a14012c6c0c9cbd054782662151809c0806' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\header.tpl',
      1 => 1557834968,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14043505695ce26a2f5a2428-26120407',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'title' => 0,
    'IMGPATH' => 0,
    'urls' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce26a2f5cff58_98129401',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce26a2f5cff58_98129401')) {function content_5ce26a2f5cff58_98129401($_smarty_tpl) {?><!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DC - <?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    <!--  Favicon  -->
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/favicon_dc.ico">
    <link rel="icon" type="image/x-icon" href="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/favicon_dc.ico">
    <!--  Link style Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Fredericka+the+Great|Special+Elite" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
    <!--  Feuilles de style  -->
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/media_queries.css">
    <link rel="stylesheet" href="assets/css/ckeditor.css">
    <!--  Link Javascript -->
    <?php echo '<script'; ?>
 src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"><?php echo '</script'; ?>
>
    <!-- Lien pour l'éditeur de texte CKEditor 5 -->
    <?php echo '<script'; ?>
 src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="assets/js/comics.js"><?php echo '</script'; ?>
>
</head>

<body>

    <header class="button-comics">
        <div class="menu-section">
            <div class="menu-toggle">
                <div class="one"></div>
                <div class="two"></div>
                <div class="three"></div>
            </div>
            <nav>
                <ul role="navigation" class="hidden">
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['home'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/menu_dc2.png" class="imgMenu" alt="img menu accueil" />Home</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['characters'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/menu_characters2.png" class="imgMenu" alt="img menu characters" />Characters</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['movies'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/menu_movie.png" class="imgMenu" alt="img menu movie" />Movies</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['comics'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/menu_comics.png" class="imgMenu" alt="img menu comics" />Comics</a></li>
                    <li><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['blog'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/menu_game2.png" class="imgMenu" alt="img menu games" />Blog</a></li>
                </ul>
            </nav>
        </div>
        
        <!-- <div>Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" title="Flaticon">
        www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
        
        <div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div> -->
        
        <div class="banner_title_personnages">
            <h1><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</h1>
        </div>
        
    </header>      <?php }} ?>
