<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 09:05:49
         compiled from "C:\wamp64\www\projet\application\views\characters.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13714807325ce26a37e40512-61707668%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '78b877390bdd719e5e902192bc3e7c7dae3b6baa' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\characters.tpl',
      1 => 1558602138,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13714807325ce26a37e40512-61707668',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce26a37e4c3a0_47504598',
  'variables' => 
  array (
    'objCharacters' => 0,
    'IMGPATH' => 0,
    'champs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce26a37e4c3a0_47504598')) {function content_5ce26a37e4c3a0_47504598($_smarty_tpl) {?>    <main class="container superman">
        <form method="post" class="form-inline my-2 my-lg-0 search">
          <input class="form-control mr-sm-2" type="search" name="keywords" placeholder="Search" aria-label="Search" value="<?php if ((isset($_POST['keywords']))) {
echo $_POST['keywords'];
}?>">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
       <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objCharacters']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
        <div class="row row_col">
            <div class="col-8">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active"> <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['champs']->value->slider;?>
/slider_img1.png" alt="First slide"> </div>
                        <div class="carousel-item"> <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['champs']->value->slider;?>
/slider_img2.png" alt="Second slide"> </div>
                    </div>
                </div>
            </div>
            <div class="col-4 img_col_border"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_personnage/border_right_perso.png" id="img_perso_right" alt="bordure de texte">
                <div class="row row-top">
                    <div class="row row-large-double">
                        <div class="col-12 title-bottom">
                            <p>Powers</p>
                        </div>
                    </div>
                    <div class="row row-large-double-bottom">
                        <div class="col-12 text1-bottom blue-perso"> 
                            <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->powers;?>
</p>
                        </div>
                    </div>
                    <div class="col-12 title-top">
                        <p>Related Characters</p>
                    </div>
                </div>
                <div class="row row-large-double-top">
                    <div class="col-12 text1-top blue-perso">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->relatedCharacters;?>
</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="row row_col">
            <div class="col-8 col-bloc-name"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_personnage/border_bottom_perso.png" id="img_perso_bottom" alt="bordure de texte" style="width: 100%;">
                <div class="row row-large-name-city">
                    <div class="col-7 col-name">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->name;?>
 - <?php echo $_smarty_tpl->tpl_vars['champs']->value->alias;?>
</p>
                    </div>
                    <div class="col-5 col-city">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->city;?>
</p>
                    </div>
                </div>
            </div>
            <div class="col-4"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/<?php echo $_smarty_tpl->tpl_vars['champs']->value->logo;?>
" id="img_perso_logo" alt="logo dc superman"> </div>
        </div>
        <?php } ?>
    </main><?php }} ?>
