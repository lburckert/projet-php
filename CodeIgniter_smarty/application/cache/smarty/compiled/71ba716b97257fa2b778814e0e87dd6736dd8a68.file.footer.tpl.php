<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 07:28:52
         compiled from "C:\wamp64\www\projet\application\views\footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3279596085ce26a2f5ea8f4-52976256%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '71ba716b97257fa2b778814e0e87dd6736dd8a68' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\footer.tpl',
      1 => 1558596530,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3279596085ce26a2f5ea8f4-52976256',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce26a2f6075b3_14853922',
  'variables' => 
  array (
    'urls' => 0,
    'IMGPATH' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce26a2f6075b3_14853922')) {function content_5ce26a2f6075b3_14853922($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_date_format')) include 'C:\\wamp64\\www\\projet\\application\\third_party\\Smarty\\plugins\\modifier.date_format.php';
?><!-- Footer -->
<footer class="container">
        <div class="row footer-list">
            <div class="col-12 col-md-4 footer-list"><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['plan'];?>
">
                    <h4>Plan du site</h4>
                </a></div>
            <div class="col-12 col-md-4 footer-list"><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['contact'];?>
">
                    <h4>Contact</h4>
                </a></div>
            <div class="col-12 col-md-4 footer-list"><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['mentions'];?>
">
                    <h4>Mentions légales</h4>
                </a></div>
        </div>
        <div class="row row-line">
            <div class="col col-md-4 footer-line top"> </div>
            <div class="col col-md-4 footer-icon">
                <div class="row">
                    <div class="col-1 bloc-vide"></div>
                    <a href="https://twitter.com/DCComics" class="col-2 footer-icon-img" target="_blank" title="Twitter"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/footer_twitter-integration.png" class="footer-img" alt="icon twitter"></a>
                    <a href="https://www.facebook.com/frdccomics" class="col-2 footer-icon-img" target="_blank" title="Facebook"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/footer_facebook-integration.png" class="footer-img" alt="icon facebook"></a>
                    <a href="https://www.youtube.com/user/DCEntertainmentTV" class="col-2 footer-icon-img" target="_blank" title="Youtube"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/footer_youtube-integration.png" class="footer-img" alt="icon youtube"></a>
                    <a href="https://www.pinterest.fr/dccomics/" class="col-2 footer-icon-img" target="_blank" title="Pinterest"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/footer_pinterest-integration.png" class="footer-img" alt="printerest"></a>
                    <a href="https://www.dccomics.com/" class="col-2 footer-icon-img" target="_blank" title="DC Official web site"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/footer_dc-integration.png" class="footer-img" alt="icon dc. comics vers site offciel"></a>
                    <div class="col-1"></div>
                </div>
            </div>
            <div class="col col-md-4 footer-line"> </div>
        </div>
        <div class="row">
            <div class="col-12 copyright-col">
                <p><span id="coryright">©</span> <?php echo smarty_modifier_date_format(time(),'%Y');?>
 DC. COMICS by L. Burckert</p>
            </div>
        </div>
</footer>

</body>

</html><?php }} ?>
