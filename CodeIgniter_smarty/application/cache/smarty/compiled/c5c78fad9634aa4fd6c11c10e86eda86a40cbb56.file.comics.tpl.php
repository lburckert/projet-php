<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 19:12:31
         compiled from "C:\wamp64\www\projet\application\views\comics.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14915030035ce3e90624f789-67368183%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c5c78fad9634aa4fd6c11c10e86eda86a40cbb56' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\comics.tpl',
      1 => 1558638689,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14915030035ce3e90624f789-67368183',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce3e90627b796_92635517',
  'variables' => 
  array (
    'objComics' => 0,
    'IMGPATH' => 0,
    'champs' => 0,
    'objTomes' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce3e90627b796_92635517')) {function content_5ce3e90627b796_92635517($_smarty_tpl) {?>    <main class="container comics">

        <div class="content">

            <!-- Tab panes -->
            <div class="tab-content">
               <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objComics']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
                <div role="tabpanel" class="tab-pane fade show active" id="injustice">
                    <div class="comics-list">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/page_comics/<?php echo $_smarty_tpl->tpl_vars['champs']->value->picture;?>
" class="img-comics" alt="image deco ligne" />
                        <p>Collection : <?php echo $_smarty_tpl->tpl_vars['champs']->value->collection;?>
<br />
                            Date de sortie premier Tome : <?php echo $_smarty_tpl->tpl_vars['champs']->value->firstDate;?>
<br />
                            EAN : <?php echo $_smarty_tpl->tpl_vars['champs']->value->ean;?>
<br />
                            Acheter : <a href="<?php echo $_smarty_tpl->tpl_vars['champs']->value->link;?>
">Cliquez pour aller vers la page de vente</a>
                        </p>
                    </div>
                    <div class="col webkit-page">
                   
                            <div class="tab-content pave-bottom" id="nav-tabContent-injustice">   
                            <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objTomes']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
                               <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->label;?>
</h6>
                                <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->resume;?>
</p>
                            <?php } ?>
                            </div>
                    </div>
                </div>
                <?php } ?>
            </div>

        </div>
    </main><?php }} ?>
