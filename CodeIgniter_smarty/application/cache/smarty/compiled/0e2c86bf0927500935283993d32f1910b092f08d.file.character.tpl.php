<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-22 19:56:15
         compiled from "C:\wamp64\www\projet\application\views\character.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12175809625ce5a02a067275-24047034%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0e2c86bf0927500935283993d32f1910b092f08d' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\character.tpl',
      1 => 1558554973,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12175809625ce5a02a067275-24047034',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce5a02a06af38_60726373',
  'variables' => 
  array (
    'objCharacter' => 0,
    'IMGPATH' => 0,
    'champs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce5a02a06af38_60726373')) {function content_5ce5a02a06af38_60726373($_smarty_tpl) {?>    <main class="container superman">
       <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objCharacter']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
        <div class="row row_col">
            <div class="col-8">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active"> <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['champs']->value->slider;?>
/slider_img1.png" alt="First slide"> </div>
                        <div class="carousel-item"> <img class="d-block w-100" src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['champs']->value->slider;?>
/slider_img2.png" alt="Second slide"> </div>
                    </div>
                </div>
            </div>
            <div class="col-4 img_col_border"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_personnage/border_right_perso.png" id="img_perso_right" alt="bordure de texte">
                <div class="row row-top">
                    <div class="row row-large-double">
                        <div class="col-12 title-bottom">
                            <p>Powers</p>
                        </div>
                    </div>
                    <div class="row row-large-double-bottom">
                        <div class="col-12 text1-bottom blue-perso"> 
                            <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->powers;?>
</p>
                        </div>
                    </div>
                    <div class="col-12 title-top">
                        <p>Related Characters</p>
                    </div>
                </div>
                <div class="row row-large-double-top">
                    <div class="col-12 text1-top blue-perso">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->relatedCharacters;?>
</p>
                    </div>
                </div>

            </div>
        </div>
        <div class="row row_col">
            <div class="col-8 col-bloc-name"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_personnage/border_bottom_perso.png" id="img_perso_bottom" alt="bordure de texte" style="width: 100%;">
                <div class="row row-large-name-city">
                    <div class="col-7 col-name">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->name;?>
 - <?php echo $_smarty_tpl->tpl_vars['champs']->value->alias;?>
</p>
                    </div>
                    <div class="col-5 col-city">
                        <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->city;?>
</p>
                    </div>
                </div>
            </div>
            <div class="col-4"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/icon/<?php echo $_smarty_tpl->tpl_vars['champs']->value->logo;?>
" id="img_perso_logo" alt="logo dc superman"> </div>
        </div>
        <?php } ?>
    </main>
<?php }} ?>
