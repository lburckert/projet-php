<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 19:19:34
         compiled from "C:\wamp64\www\projet\application\views\movies.tpl" */ ?>
<?php /*%%SmartyHeaderCode:6763717645ce3c5978e2b12-57488370%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '596120bb81da066855887142edcbdb963fa3a573' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\movies.tpl',
      1 => 1558639168,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '6763717645ce3c5978e2b12-57488370',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce3c597905752_44904530',
  'variables' => 
  array (
    'objMovies' => 0,
    'IMGPATH' => 0,
    'champs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce3c597905752_44904530')) {function content_5ce3c597905752_44904530($_smarty_tpl) {?>    <!-- Contenu -->
    <main class="container series-container table-movie">
        <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objMovies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
        <div class="row global-line">
            <div class="col-12 bloc-left">
                <div class="row interne-line spe">
                    <div class="col-6 center-img"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_movies/<?php echo $_smarty_tpl->tpl_vars['champs']->value->picture;?>
" class="imgSeries" alt="" /> </div>
                    <div class="col-6 bloc-sup">
                        <h5><?php echo $_smarty_tpl->tpl_vars['champs']->value->label;?>
</h5>
                        <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->date;?>
</h6>
                        <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->alias;?>
</h6> </div>
                </div>
                <div class="row interne-line separation">
                    <p><?php echo $_smarty_tpl->tpl_vars['champs']->value->resume;?>
</p>
                </div>
            </div>
        </div>
        <?php } ?>
    </main><?php }} ?>
