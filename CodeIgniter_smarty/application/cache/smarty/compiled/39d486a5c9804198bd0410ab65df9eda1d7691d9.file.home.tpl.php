<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-22 13:49:28
         compiled from "C:\wamp64\www\projet\application\views\home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:15748141025ce26a2f5da2f3-75864363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39d486a5c9804198bd0410ab65df9eda1d7691d9' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\home.tpl',
      1 => 1558532966,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '15748141025ce26a2f5da2f3-75864363',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce26a2f5e09f7_80340039',
  'variables' => 
  array (
    'IMGPATH' => 0,
    'homeArticle' => 0,
    'champs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce26a2f5e09f7_80340039')) {function content_5ce26a2f5e09f7_80340039($_smarty_tpl) {?>    <main class="container home">
        <div class="row row-line-accueil">
           
           
           <div class="col-6 bottom-img-acceuil"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_accueil/border_texte_right.png" id="img_accueil_right" alt="bordure de texte">
                <div class="row">
                    <div class="col col-lg-12 title-top-accueil">
                        <p>Histoire de DC. Comics</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-12 text1-top-accueil">
                        <p>DC Entertainment, la maison des marques emblématiques DC (Superman, Batman, Green Lantern, Wonder Woman, The Flash), Vertigo (Sandman, Fables) et MAD, est la division créative chargée d'intégrer de manière stratégique son contenu dans Warner Bros. Entertainment et Time Warner. DC Entertainment collabore avec de nombreuses divisions clés de Warner Bros. pour diffuser ses histoires et ses personnages sur tous les supports, y compris, sans toutefois s'y limiter, les films, la télévision, les produits grand public, les divertissements à domicile et les jeux interactifs. En publiant des milliers de bandes dessinées, de romans illustrés et de magazines chaque année, DC Entertainment est l’un des plus grands éditeurs de bandes dessinées de langue anglaise au monde.</p>
                    </div>
                </div>
            </div>
            
            <div class="col-1"> </div>
            
            <div class="col-5"> 
           
                <div class="container blog-container table-movie">
                    <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['homeArticle']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
                    <div class="row global-line">
                        <div class="col-12 bloc-left">
                            <div class="row interne-line spe">
                                <div class="col-6 center-img"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_blog/<?php echo $_smarty_tpl->tpl_vars['champs']->value->picture;?>
" class="imgSeries" alt="" /> </div>
                            </div>
                            <div class="row interne-line separation">
                                <p>
                                <h5><?php echo $_smarty_tpl->tpl_vars['champs']->value->label;?>
</h5>
                                <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->createDate;?>
</h6>
                                <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->pseudo;?>
</h6>
                                <?php echo $_smarty_tpl->tpl_vars['champs']->value->resume;?>

                                </p>
                            </div>
                        </div>

                    </div>
                    <?php } ?>
                </div>
            </div>
            
            
        </div>
    </main><?php }} ?>
