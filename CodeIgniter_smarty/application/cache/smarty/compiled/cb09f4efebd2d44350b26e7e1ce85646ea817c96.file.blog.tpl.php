<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 15:24:38
         compiled from "C:\wamp64\www\projet\application\views\blog.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14339349855ce4f21420de45-43003678%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'cb09f4efebd2d44350b26e7e1ce85646ea817c96' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\blog.tpl',
      1 => 1558625058,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14339349855ce4f21420de45-43003678',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce4f21424d2f7_34750954',
  'variables' => 
  array (
    'urls' => 0,
    'IMGPATH' => 0,
    'objBlog' => 0,
    'champs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce4f21424d2f7_34750954')) {function content_5ce4f21424d2f7_34750954($_smarty_tpl) {?>    <!-- Contenu -->
    <main class="container profil-container" >
        <div class="row profile">
            <div class="col-10 "></div>
            <div class="col-1 "> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['login'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/my-icons-collection/log-in.png" class="imgLogin" alt="icon de login, logout" /></a>
            </div>
            <div class="col-1 "> 
                <a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['profile'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/my-icons-collection/profile.png" class="imgProfile" alt="icon d'accès au profile" /></a>
            </div>
        </div>
    </main>
    
    <main class="container link-new-article-container" >
        <div class="row ">
            <div class="col-6 text-link-create-article"> 
                <li class="create-article"><a href="<?php echo $_smarty_tpl->tpl_vars['urls']->value['login'];?>
">Créez votre propre article !</a></li>
            </div>
            <div class="col-6 "> 
                <a href="#" class="tootltip" data-toggle="tooltip" data-placement="bottom" title="Ici vous pourrez créer votre propre article et le publier sur le Blog. 
Pour accéder à la page de création d'un article il vous faudra vous connecter"><img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/my-icons-collection/question.png" class="icon-help-article" alt="icon d'aide" /></a>
                <!-- 
                AIDE EN LIGNE ! ---------------------------------------------------------------------------------------          
                Texte lors du survol sur l'icon : Ici vous pourrez créer votre propre article et le publier sur le Blog. 
                Pour accéder à la page de création d'un article il vous faudra vous connecter -->
            </div>
        </div>
    </main>
    
    <main class="container blog-container table-movie">
       
    <form method="post" class="form-inline my-2 my-lg-0 search">
      <input class="form-control mr-sm-2" type="search" name="keywords" placeholder="Search" aria-label="Search" value="<?php if ((isset($_POST['keywords']))) {
echo $_POST['keywords'];
}?>">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
       
        <?php  $_smarty_tpl->tpl_vars['champs'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['champs']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['objBlog']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['champs']->key => $_smarty_tpl->tpl_vars['champs']->value) {
$_smarty_tpl->tpl_vars['champs']->_loop = true;
?>
        <div class="row global-line">
            <div class="col-8 bloc-left">
                <div class="row interne-line spe">
                    <div class="col-6 center-img"> <img src="<?php echo $_smarty_tpl->tpl_vars['IMGPATH']->value;?>
/img_blog/<?php echo $_smarty_tpl->tpl_vars['champs']->value->picture;?>
" class="imgSeries" alt="" /> </div>
                </div>
                <div class="row interne-line separation">
                    <p>
                    <h5><?php echo $_smarty_tpl->tpl_vars['champs']->value->label;?>
</h5>
                    <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->createDate;?>
</h6>
                    <h6><?php echo $_smarty_tpl->tpl_vars['champs']->value->pseudo;?>
</h6>
                    <?php echo $_smarty_tpl->tpl_vars['champs']->value->resume;?>

                    </p>
                </div>
            </div>

        </div>
        <?php } ?>
    </main><?php }} ?>
