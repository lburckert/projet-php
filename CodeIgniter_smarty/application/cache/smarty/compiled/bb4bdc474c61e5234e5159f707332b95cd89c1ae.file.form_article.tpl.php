<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 14:07:16
         compiled from "C:\wamp64\www\projet\application\views\form_article.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3443605625ce52929477c43-98095531%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb4bdc474c61e5234e5159f707332b95cd89c1ae' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\form_article.tpl',
      1 => 1558620401,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3443605625ce52929477c43-98095531',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce52929489c90_18815273',
  'variables' => 
  array (
    'urls' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce52929489c90_18815273')) {function content_5ce52929489c90_18815273($_smarty_tpl) {?>    <main class="container container-bloc contact new-article">
       <div class="form">
        <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['urls']->value['articleNew'];?>
">
            <div class="row bloc-contact">
                <div class="col-6 border-contact">
                    <div class="row">
                        <div class="col-12 col-fieldset">
                            <fieldset class="border p-2">
                                <legend class="title-contact w-auto">Infos de l'article</legend>
                                <!-- le titre du fieldset-->
                                <label for="titre" class="space">Titre de l'article :</label>
                                <input type="text" name="label" id="titre" placeholder="La bataille entre Flash et Zoom" required class="space" />
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col-6 border-contact">
                    <div class="row">
                        <div class="col-12 bloc-text">
                            <label for="ameliorer" class="title-contact">Placez votre texte ici</label>
                            <textarea name="resume" id="editor" onkeyup="javascript:MaxLengthTextarea(this, 250);" ></textarea>
                        </div>
                    </div>
                    <!-- Appel de la fonction JS EDITOR !!! -->
                    <?php echo '<script'; ?>
>
                        ClassicEditor.create(document.querySelector('#editor'), {
                            toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote', 'imageUpload']
                            , heading: {
                                options: [
                                    {
                                        model: 'paragraph'
                                        , title: 'Paragraph'
                                        , class: 'ck-heading_paragraph'
                                    }
                                    , {
                                        model: 'heading1'
                                        , view: 'h1'
                                        , title: 'Heading 1'
                                        , class: 'ck-heading_heading1'
                                    }
                                    , {
                                        model: 'heading2'
                                        , view: 'h2'
                                        , title: 'Heading 2'
                                        , class: 'ck-heading_heading2'
                                    }
                                        ]
                            }
                        }).then(newEditor => {
                            window.myEditor = newEditor;
                            newEditor.model.document.on('change:data', () => {
                                let maxlength = 450
                                let data = window.myEditor.getData();
                                if (data.length >= maxlength) {
                                    data = data.substring(0, maxlength);
                                    alert('Le texte ne doit pas dépasser ' + maxlength + ' caractères!');
                                }
                            });
                        }).catch(error => {
                            console.error(error);
                        });
                    <?php echo '</script'; ?>
>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <input type="submit" value="Envoyer !" class="space button-send" /> </div>
            </div>
        </form>
    </main>
</div><?php }} ?>
