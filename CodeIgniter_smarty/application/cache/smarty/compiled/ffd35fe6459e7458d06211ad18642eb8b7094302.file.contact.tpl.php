<?php /* Smarty version Smarty-3.1.21-dev, created on 2019-05-23 08:57:25
         compiled from "C:\wamp64\www\projet\application\views\contact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12505390145ce64bf0ed90a6-41055164%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ffd35fe6459e7458d06211ad18642eb8b7094302' => 
    array (
      0 => 'C:\\wamp64\\www\\projet\\application\\views\\contact.tpl',
      1 => 1558601839,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12505390145ce64bf0ed90a6-41055164',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.21-dev',
  'unifunc' => 'content_5ce64bf0f01706_95404831',
  'variables' => 
  array (
    'urls' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ce64bf0f01706_95404831')) {function content_5ce64bf0f01706_95404831($_smarty_tpl) {?>    <main class="container container-bloc border-contact contact">
        <div class="row bloc-contact">
            <div class="col-12 col-fieldset">
                <fieldset class="border p-2">
                    <legend class="title-contact w-auto">Nos coordonnées</legend>
                    <h5>WB Games Montréal 888 Boulevard de Maisonneuve E<br/>
                Montréal, QC H2L 4M8, Canada</h5>
                    <h5>+1 514-395-4745</h5> </fieldset>
            </div>
        </div>
        <div class="row bloc-contact">
            <div class="col-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2795.6775018692615!2d-73.5613583844408!3d45.51657027910161!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cc91bb2589124bb%3A0x25949bac8df91a78!2sWB+Games+Montr%C3%A9al!5e0!3m2!1sfr!2sfr!4v1547133320200" frameborder="0" allowfullscreen class="maps"></iframe>
            </div>
        </div>
    </main>
    <main class="container container-bloc contact">
        <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['urls']->value['contact'];?>
">
            <div class="row bloc-contact">
                <div class="col-12 border-contact">
                    <div class="row">
                        <div class="col-12 col-fieldset">
                            <fieldset class="border p-2">
                                <legend class="title-contact w-auto">Vos coordonnées</legend>
                                <!-- le titre du fieldset-->
                                <label for="pseudo" class="space">Pseudo :</label>
                                <input type="text" name="pseudo" id="pseudo" placeholder="Xx_flash_xX" maxlength="18" required class="space" />
                                <br/>
                                <label for="email" class="space">E-mail :</label>
                                <input type="email" name="email" id="email" placeholder=".com" required class="space" />
                                <br/>
                                <label for="tel" class="space">Telephone :</label>
                                <input type="tel" name="tel" id="tel" placeholder="1 514..." class="space" />
                                <br/>
                                <label for="date" class="space">Date de naissance :</label>
                                <input type="date" name="date" id="date" placeholder="24/02/98" class="space" /> 
                            </fieldset>
                        </div>
                    </div>
                    
                   <div class="col-12 border-contact">
                    <div class="row">
                        <div class="col-12 bloc-text area">
                             <label for="content" class="title-contact">Placez votre texte ici</label>
                            
                            <textarea name="content" id="editor" onkeyup="javascript:MaxLengthTextarea(this, 250);"></textarea>
                        </div>
                    </div>
                    <!-- Appel de la fonction JS EDITOR !!! -->
                     <?php echo '<script'; ?>
>
                        ClassicEditor
                            .create(document.querySelector('#editor'), {
                                    toolbar: ['heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote'],
                                    heading: {
                                        options: [
                                            { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                                            { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                                            { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                                        ]
                                    }
                                })
                            .then( newEditor => {
                                window.myEditor = newEditor;
                                
                                newEditor.model.document.on( 'change:data', () => {
                                    let maxlength = 450
                                    let data = window.myEditor.getData();

                                    if(data.length >= maxlength) {
                                        data = data.substring(0, maxlength);
                                        alert('Le texte ne doit pas dépasser ' + maxlength + ' caractères!');
                                    }
                                    
                                } );
                            } )
                            .catch(error => {
                                console.error(error);
                            });
                    <?php echo '</script'; ?>
>
                        
                    </div>
                    <div class="row">
                        <div class="col-4 dernier-bloc">
                            <label for="pays" class="space">Votre pays :</label>
                            <select name="pays" id="pays" class="space">
                                <option value="etats-unis">Etats-Unis</option>
                                <option value="canada" selected>Canada</option>
                                <!-- selected = selectionné par défaut-->
                                <option value="royaume-unis">Royaume-Unis</option>
                                <option value="france">France</option>
                                <option value="russie">Russie</option>
                                <option value="japon">Japon</option>
                                <option value="australie">Australie</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="submit" value="Envoyer" class="space button-send" />
                        </div>
                    </div>

                </div>
            </div>
        </form>
        
    </main><?php }} ?>
